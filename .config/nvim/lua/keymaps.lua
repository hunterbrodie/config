vim.g.mapleader = ' '
local map = function(mode, lhs, rhs)
	vim.api.nvim_set_keymap(
		mode,
		lhs,
		rhs,
		{
			noremap = true
		}
	)
end

 -- Tab mappings
map('n', '<leader>1', '1gt')
map('n', '<leader>2', '2gt')
map('n', '<leader>3', '3gt')
map('n', '<leader>4', '4gt')
map('n', '<leader>5', '5gt')
map('n', '<leader>6', '6gt')
map('n', '<leader>7', '7gt')
map('n', '<leader>8', '8gt')
map('n', '<leader>9', '9gt')
map('n', '<leader>0', ':tablast<cr>')

 -- Remove training wheels
map('n', '<up>', '<nop>')
map('n', '<down>', '<nop>')
map('n', '<left>', '<nop>')
map('n', '<right>', '<nop>')
