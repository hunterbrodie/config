vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
	
	 -- Manage itself
	use 'wbthomason/packer.nvim'
	
	 -- Theme
	use
	{
		'npxbr/gruvbox.nvim',
		requires =
		{
			'rktjmp/lush.nvim'
		}
	}

	 -- Dashboard
	use 
	{
		'nvim-telescope/telescope.nvim',
		requires = 
		{
			'nvim-lua/popup.nvim',
			'nvim-lua/plenary.nvim'
		}
	}
	use 'glepnir/dashboard-nvim'

	 -- LSP
	use 'neovim/nvim-lspconfig'
	use 'nvim-lua/completion-nvim'

	 -- QoL
	use 'airblade/vim-gitgutter'
	use 'hoob3rt/lualine.nvim'
	use 'crispgm/nvim-tabline'
	use 'windwp/nvim-autopairs'
	
	 -- Note Taking
	use 'jbyuki/nabla.nvim'
end)
