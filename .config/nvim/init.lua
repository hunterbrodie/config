 -- Default Misc
vim.o.termguicolors = true
vim.o.syntax = 'on'
vim.o.smartcase = true
vim.o.showmode = false
vim.bo.swapfile = false
vim.o.hidden = true
vim.o.completeopt='menuone,noinsert,noselect'

 -- Layout Settings
vim.bo.autoindent = true
vim.bo.smartindent = true
vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = false
vim.wo.number = true
vim.wo.signcolumn = 'yes'

 -- Required Files
require('plugins')
require('keymaps')
require('brackets')

 -- Theme
vim.o.background = "dark"
vim.cmd[[colorscheme gruvbox]]

 -- QoL Misc
require'lualine'.setup
{
	options = 
	{
		theme = 'gruvbox'
	}
}
require'tabline'.setup{}

 -- Notetaking
vim.cmd[[nnoremap <F5> :lua require("nabla").action()<CR>]]

 -- Autocomplete tab cycling
vim.cmd[[inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"]]
vim.cmd[[inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"]]

 -- Language support
require'lspconfig'.rls.setup
{
	on_attach=require'completion'.on_attach,
}

require'lspconfig'.racket_langserver.setup
{
	on_attach=require'completion'.on_attach,
}

require'lspconfig'.texlab.setup
{
	on_attach=require'completion'.on_attach,
}
