export MOZ_ENABLE_WAYLAND=1 firefox
export GTK_THEME=Adwaita:dark
export _JAVA_AWT_WM_NONREPARENTING=1
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORMTHEME=gtk2
export XDG_CURRENT_DESKTOP=sway

export EDITOR=nvim
