########
# Misc #
########

bar
{
	swaybar_command waybar
}

#############
# Variables #
#############

# Misc
set $mod Mod4
set $term alacritty

# Paths
set $lock ~/.config/sway/lock.sh

# Directions
set $left h
set $right l
set $up k
set $down j

# Term
bindsym $mod+Return exec $term

# Application Launcher
bindsym $mod+d exec wofi

###########
# Display #
###########

# Wallpaper
output "*" bg ~/.config/sway/wallpaper.png fill

# Hi-DPI
output eDP-1 scale 2

# Second Monitor
output DP-3 res 2560x1440

###########
# Windows #
###########

# Hide Borders
default_border none

# Gaps
smart_gaps on
gaps inner 20
gaps outer 10

# Resize
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

# Screenshot
bindsym $mod+p exec grimshot --notify copy
bindsym $mod+Shift+p exec grimdshot --notify save
bindsym Print exec grimshot --notify copy
bindsym Shift+Print exec grimshot --notify save

############
# Keybinds #
############

# Autostart Numlock
input * xkb_numlock enable

# Audio
bindsym --locked XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym --locked XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle

# Playback
bindsym --locked XF86PickupPhone exec playerctl previous
bindsym --locked XF86HangupPhone exec playerctl play-pause
bindsym --locked XF86Favorites exec playerctl next
bindsym --locked XF86AudioPlay exec playerctl play-pause
bindsym --locked XF86AudioNext exec playerctl next
bindsym --locked XF86AudioPrev exec playerctl previous

# Brightness
bindsym XF86MonBrightnessUp exec brightnessctl set 5%+
bindsym xF86MonBrightnessDown exec brightnessctl set 5%-

# Kill Focused Window
bindsym $mod+Shift+q kill

# Change Focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# Move Focused Window
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# Change Split Orientation
bindsym $mod+b split h
bindsym $mod+v split v

# Enter Fullscreen
bindsym $mod+f fullscreen toggle

# Exit Sway
bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -B 'Yes, exit sway' 'swaymsg exit'

# Switch Workspace
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# Lock
bindsym $mod+Shift+c exec $lock

# Reload
bindsym $mod+Shift+r exec swaymsg reload

# SystemD
exec_always "systemctl --user import-environment; systemctl --user start sway-session.target"

# Mako
exec mako
