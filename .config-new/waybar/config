[
	{
		"name": "topbar",
		"layer": "top",
		"position": "top",
		"height": 26,
		"modules-left": [
			"custom/music",
			"custom/prev",
			"custom/playpause",
			"custom/next"
		],
		"modules-center": [
			"idle_inhibitor"
		],
		"modules-right": [
			"pulseaudio",
			"network",
			"cpu",
			"memory",
			"disk",
			"temperature",
			"battery",
			"tray",
			"custom/clock"
		],
		"idle_inhibitor": {
			"format": "{icon}",
			"format-icons": {
				"activated": "",
				"deactivated": ""
			},
			"tooltip": false
		},
		"tray": {
			"icon-size": 14,
			"spacing": 10
		},
		"clock": {
			"format": "{:%b %d, %Y at %H:%M}",
			"tooltip": false
		},
		"custom/clock": {
			"exec": "date +'%b %d, %H:%M'",
			"interval": 10
		},
		"cpu": {
			"format": "{usage}% ",
			"on-click": "alacritty -e htop"
		},
		"memory": {
			"interval": 30,
			"format": "{used:0.1f}/{total:0.1f}GB ",
			"tooltip": false
		},
		"disk": {
			"interval": 30,
			"format": "{used}/{total} ",
			"path": "/"
		},
		"temperature": {
			"thermal-zone": 2,
			"critical-threshold": 80,
			"format-critical": "{temperatureC}°C {icon}",
			"format": "{temperatureC}°C {icon}",
			"format-icons": [
				"",
				"",
				""
			],
			"tooltip": false
		},
		"network": {
			"format-wifi": "{essid} ({signalStrength}%) ",
			"format-ethernet": "{ifname}: {ipaddr} ",
			"format-disconnected": "Disconnected ⚠",
			"on-click": "alacritty -e iwctl",
			"tooltip": false
		},
		"pulseaudio": {
			"format": "{volume}% {icon}",
			"format-bluetooth": "{volume}% {icon}",
			"format-muted": "",
			"format-icons": {
				"headphones": "",
				"default": [
					"",
					"",
					"🔊"
				]
			},
			"on-click": "alacritty -e pulsemixer"
		},
		"battery": {
			"states": {
				"warning": 30,
				"critical": 15
			},
			"format": "{capacity}% {icon}",
			"format-charging": "{capacity}% ",
			"format-plugged": "{capacity}% ",
			"format-alt": "{time} {icon}",
			"format-icons": [
				"",
				"",
				"",
				""
			]
		},
		"custom/music": {
			"format": "{}",
			"max-length": 80,
			"exec": "playerctl --player spotify,cmus -F metadata --format '{{artist}} - {{title}}'"
		},
		"custom/next": {
			"format": "{icon}",
			"exec": "playerctl --player spotify,cmus -F metadata --format '{\"alt\": \"{{status}}\"}'",
			"return-type": "json",
			"format-icons": {
				"Playing": "",
				"Paused": ""
			},
			"on-click": "playerctl --player spotify,cmus next",
			"tooltip": false
		},
		"custom/playpause": {
			"format": "{icon}",
			"return-type": "json",
			"exec": "playerctl --player spotify,cmus -F metadata --format '{\"alt\": \"{{status}}\"}'",
			"format-icons": {
				"Playing": "",
				"Paused": ""
			},
			"on-click": "playerctl --player spotify,cmus play-pause",
			"tooltip": false
		},
		"custom/prev": {
			"format": "{icon}",
			"return-type": "json",
			"exec": "playerctl --player spotify,cmus -F metadata --format '{\"alt\": \"{{status}}\"}'",
			"format-icons": {
				"Playing": "",
				"Paused": ""
			},
			"on-click": "playerctl --player spotify,cmus previous",
			"tooltip": false
		},
		"custom/lichess": {
			"format": " {}",
			"exec": "$HOME/.config/waybar/lichess-rating hunterbrodie rapid",
			"tooltip": false
		}
	},
	{
		"name": "leftbar",
		"layer": "bottom",
		"position": "left",
		"modules-left": [
			"sway/workspaces"
		],
		"sway/workspaces": {
			"disable-scroll": true,
			"all-outputs": true,
			"format": "  {name}  "
		}
	}
]
