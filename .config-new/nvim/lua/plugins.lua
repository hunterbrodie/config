vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'

	use
	{
		'npxbr/gruvbox.nvim',
		requires =
		{
			'rktjmp/lush.nvim'
		}
	}

	 -- LSP
	use 'neovim/nvim-lspconfig'
--	use 'nvim-lua/completion-nvim'
  
  -- Autocomplete
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/nvim-cmp'

  -- Snippits
  use 'L3MON4D3/LuaSnip'
  use 'saadparwaiz1/cmp_luasnip'

	 -- QoL
	use 'windwp/nvim-autopairs'
	use 'hoob3rt/lualine.nvim'
end)
